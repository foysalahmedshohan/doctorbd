<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () {
//     return view('welcome');
// });


// Route::get('/search/{search}',function($search){
// 	return App\Hospital::where('hname','like','%'.$search.'%')->pluck('hname')->toArray();
// });




// //doctor
// Route::get('/doctor', function () {
//     return view('admin.doctor.create');
// });
// Route::get('/doctor/edit', function () {
//     return view('admin.doctor.edit');
// });

// //
// //hospital

// Route::get('/hospital', function () {
//     return view('admin.hospital.create');
// });
// Route::get('/hospital/edit', function () {
//     return view('admin.hospital.edit');
// });
// use Illuminate\Support\Facades\Auth;

// //auth routes
// Auth::routes();


Route::group(['prefix'=>'dashboard', 'middleware'=>'dashboard', 'namespace'=>'Admin'], function(){
    

  Route::get('/', 'DashboardController@index')->name('admin.dashboard');

  // Route::resource('categories', 'CategoryController');

  Route::resource('user', 'UserController');
  Route::get('/role', 'UserController@getRole')->name('role');

  Route::resource('ambulance', 'AmbulanceController')->middleware('roles:admin|ambulance|hospital');

  Route::get('doctor/profile', 'DoctorController@doctorProfile')->name('doc_profile')->middleware('roles:doctor');
  Route::resource('doctor', 'DoctorController');
  //Route::post('doctor/profile', 'DoctorController@storeDoctorProfile')->name('sotre_doc_profile');

  Route::resource('seat', 'SeatController');

  Route::resource('seat', 'SeatController');

  Route::resource('hospital', 'HospitalController');

  // Route::get('/admin','Admin/DashboardController@home');

  Route::resource('education', 'EducationController');
  Route::resource('test', 'TestController');
  Route::resource('cost', 'CostController');

  //donor routes for admin
  Route::resource('/donor', 'DonorController')->middleware('roles:admin');

});

/*===========Start Visitor routes=================*/

Route::get('/find-ambulance', 'AllSearchController@ambulanceSearch')->name('find-amb');
Route::get('/find-donor', 'AllSearchController@donorSearch')->name('find-donor');

// Route::get('/amb_ajax','AllSearchController@ambulanceAjaxSearch' )->name('amb_ajax');
Route::get('/doctor_ajax','AllSearchController@doctorSearch' )->name('doc_search');

Route::get('/reg', function () {
    return view('visitor.donor_search');
});


Route::get('/test', function() {

});//api

/*===============End Visitor rotues===============*/


//visitor

Route::get('/', function () {
    return view('visitor.index');
});


Route::get('/a', function () {
    return view('visitor.ambsearch');
});


// Route::get('/b', function () {
//     return view('visitor.bsearch');
// });


Route::get('/d', function () {
    return view('visitor.hsearch');
});

Route::get('/do', function () {
    return view('visitor.dsearch');
});

// Route::get('/l', function () {
//     return view('visitor.login');
// });

// Route::get('/sa', function () {
//     return view('visitor.singleamb');
// });


Route::get('/sd', function () {
    return view('visitor.singledoctor');
});

Route::get('/sh', function () {
    return view('visitor.singlehospital');
});


// Route::get('/e', function () {
//     return view('visitor.education');
// });

// Route::get('/reg', function () {
//     return view('visitor.donor');
// });

//Blood donor Auths

Route::group(['prefix' => 'donor'], function () {
  Route::get('/login', 'DonorAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'DonorAuth\LoginController@login');
  Route::post('/logout', 'DonorAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'DonorAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'DonorAuth\RegisterController@register');

  Route::post('/password/email', 'DonorAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'DonorAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'DonorAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'DonorAuth\ResetPasswordController@showResetForm');
});

//Dashboard auth routes
Route::group(['prefix' => 'dashboard'], function () {
  Route::get('/login', 'DashboardAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'DashboardAuth\LoginController@login');
  Route::post('/logout', 'DashboardAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'DashboardAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'DashboardAuth\RegisterController@register');

  Route::post('/password/email', 'DashboardAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'DashboardAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'DashboardAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'DashboardAuth\ResetPasswordController@showResetForm');
});
