<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('donor')->user();

    //dd($users);
    $donor = App\Donor::where('id', auth()->id())->first();

    // dd($donor->dob);

    return view('donor.donor_profile', compact('donor'));
    // return view('donor.home');

})->name('home');

