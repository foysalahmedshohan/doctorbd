@extends('admin.layout.master')


@section('content')


<div class="container" style="width: 100%;">
  <h2 style="margin-left: 30px;">Add Seat Rent</h2>
  <hr>
  <br>
  
    <div class="row">
<!-- 
       @if(Session::has('success'))
              <div class="">
                <h5 style="color: green; ;margin-left: 15px; margin-top: -1px;"> {{Session::get('success')}}</h5> 
              </div>
              @endif -->

      <form method="post" action=" " enctype="multipart/form-data">
          {{csrf_field()}}

      <div class="col-md-6">

      <div class="form-group">
          <label for="user">Seat Name:</label>
          <input type="text" class="form-control" id="Name" value="{{old('sname')}}" name="sname">
      </div>                       
                                
       <div class="btn-group">
          <br>
        <button type="submit" class="btn btn-success">Submit</button>
        <button type="reset" class="btn btn-primary">Reset</button>
          
        </div>

    </div>

       
    <div class="col-md-6">
        

      <div class="form-group">
          <label for="user">Seat Cost:</label>
          <input type="number" class="form-control" id="Phondde" value="{{old('scost')}}" name="scost">
      </div>
          
    </div>            
    </form>

    </div>  

</div>          
@stop

