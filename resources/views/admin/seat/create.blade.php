@extends('admin.layout.master')


@section('content')


<div class="container" style="width: 100%;">
	<h2 style="margin-left: 30px;">Add Cabin</h2>
	<hr>
	<br>
  
 	 	<div class="row">
 
 	 		 @if(Session::has('success'))
              <div class="">
                <h5 style="color: green; ;margin-left: 15px; margin-top: -1px;"> {{Session::get('success')}}</h5> 
              </div>
              @endif 

 	 		<form method="post" action="" enctype="multipart/form-data">
        	{{csrf_field()}}

	    <div class="col-md-6">

			<div class="form-group">
				  <label for="user">ICU:</label>
				  <input type="text" class="form-control" value="{{old('icu')}}" id="Name" name="icu">
			</div>         
		    

		   <div class="form-group">
				  <label for="user">CCU:</label>
				  <input type="text" class="form-control" value="{{old('ccu')}}" id="Name" name="ccu">
			</div>  

         	<div class="form-group">
				  <label for="user">HDU:</label>
				  <input type="text" class="form-control" value="{{old('hdu')}}" id="Name" name="hdu">
			</div>  
			

			 <div class="btn-group">
         	<br>
			  <button type="submit" class="btn btn-success">Submit</button>
			  <button type="reset" class="btn btn-primary">Reset</button>
				  
		    </div>

		</div>
        
		<div class="col-md-6">

			<div class="form-group">
				  <label for="user">NICU:</label>
				  <input type="text" class="form-control" value="{{old('nicu')}}" id="Name" name="nicu">
			</div>  	 	

			<div class="form-group">
				  <label for="user">PICU:</label>
				  <input type="text" class="form-control" value="{{old('picu')}}" id="Name" name="picu">
			</div>  						
		      
		</div>  		
  		
		</form>		

		</div>	

</div>			    




@stop