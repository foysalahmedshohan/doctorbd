@extends('visitor.layout.master')


@section('content')
<!-- single doctor page start  -->
<div class="single-doctor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="single-doc-top text-center">
          <img src="assets/img/singledoc.png" alt="">
          <h2>Dr.Md.Taijul islam</h2>
          <h3>Consultant, Dept. of Neonatalogy, Medicare Diagnostic & Hospital Kamrangichar Diabetic Centre</h3>
          <h4><span>Neonatology (New Born Issues)</span></h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
       <div class="single-doc-left text-center">
        <h2><i class="fa fa-stethoscope" aria-hidden="true"></i>Specialist in</h2>
        <span><i class="fa fa-heart" aria-hidden="true"></i></span>
        <h4>Neonatology (New Born Issues)</h4>
        <h5>MBBS, MD</h5>
        <h4>Practicing in Neonatalogy (New born baby Issue)PGT (Child), FCPS (Part-1)</h4>
       </div>
       <div class="single-doc-lb text-center">
         <h2><i class="fa fa-location-arrow" aria-hidden="true"></i>Chambers</h2>
         <h3>Medicare Diagnostic & Hospital Kamrangirchar Diabetic Center, Main Branch</h3>
         <p>Medicare Diagnostic & Hospital Kamrangirchar Diabetic Center, Main Branch, Khalifaghat Chowrasta (Chan Masjid Rd), Kamrangirchar, Dhaka</p>
         <li>New patient <span>500tk</span></li>
       </div>
      </div>
      <div class="col-md-6">
        <div class="chamber-lacation">
          <h2><i class="fa fa-location-arrow" aria-hidden="true"></i>Chamber Location</h2>
           <div class="map">
             <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.8445831279155!2d90.37948291428786!3d23.752920984588133!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8ae4e52eabd%3A0x113b1873c9a9c2c1!2sSquare+Hospital!5e0!3m2!1sen!2sbd!4v1548296476727" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
           </div>
        </div>

         <!-- book an appoinment -->
         <div class="appoinment-area">
            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Book an Appoinment</button>
            <div id="demo" class="collapse">
            <h2>Request for an appoinment</h2>
              <form action="">
                <table>
                <tr><td>have you visited this doctor before?</td><td><input type="radio">yes <input type="radio">No</td></tr>
                <tr><td>Mobile No:</td><td><input type="text" placeholder="+8801xxxxxxxx"></td></tr>
                <tr><td></td><td><input type="submit" value="Requst for appoinment"></td></tr>
              </table>
              </form>
            </div>
             </div>
            </div>
          </div>
        </div>
      </div>
      <!-- footer-area -->
@stop