@extends('visitor.layout.master')


@section('content')
 <!-- Doctor Search area start -->
 <section id="sectionbg">
   <h1>I'm looking for a Hospital.</h1>
   <div class="container">
     <div class="row">
       <div class="col-md-2">   
       </div>
       <div class="col-md-8">
         <div class="doctor-srchbar">
           <form class="form-inline">
            <label class="sr-only" for="inlineFormInputName2">City</label>
            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Enter City">

            <label class="sr-only" for="inlineFormInputGroupUsername2">Area</label>
            <div class="input-group mb-2 mr-sm-2">
              <div class="input-group-prepend">
              </div>
              <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Area">
            </div>
              <label class="sr-only" for="inlineFormInputGroupUsername2">Deases or symtoms</label>
            <div class="input-group mb-2 mr-sm-2">
              <div class="input-group-prepend">
              </div>
              <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Deases/symtoms">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Search</button>
          </form>
         </div>
       </div>
       <div class="col-md-2"></div>
     </div>
   </div>
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="doctor-slide-area" >
           <h3>You also can choose hospital form below</h3>
           <!-- Hospital-list start-->
             <div class="container">
               <div class="row" style="margin-top:22px;">
                <div class="hospital-slider">
                 <div class="single-hospital text-center">
                   <div class="h-hospital-content-top">
                     <img src="{{asset('visitor/img/hospital/hospital1.jpg')}}" alt="">
                   </div>
                   <div class="h-hospital-content-bottom">
                     <h2>Square Hospital</h2>
                     <p>West Panthopath, Dhaka</p>
                     <p><i class="fa fa-mobile"></i><span>Contact:</span>+8801939886956</p>
                     <p><i class="fa fa-envelope"></i><span>Mail:</span>squarehospital@gmail.com</p>
                   </div>
                 </div>
                 <div class="single-hospital text-center">
                   <div class="h-hospital-content-top">
                     <img src="{{asset('visitor/img/hospital/hospital1.jpg')}}" alt="">
                   </div>
                   <div class="h-hospital-content-bottom">
                     <h2>Square Hospital</h2>
                     <p>West Panthopath, Dhaka</p>
                     <p><i class="fa fa-mobile"></i><span>Contact:</span>+8801939886956</p>
                     <p><i class="fa fa-envelope"></i><span>Mail:</span>squarehospital@gmail.com</p>
                   </div>
                 </div>
                 <div class="single-hospital text-center">
                   <div class="h-hospital-content-top">
                     <img src="{{asset('visitor/img/hospital/hospital1.jpg')}}" alt="">
                   </div>
                   <div class="h-hospital-content-bottom">
                     <h2>Square Hospital</h2>
                     <p>West Panthopath, Dhaka</p>
                     <p><i class="fa fa-mobile"></i><span>Contact:</span>+8801939886956</p>
                     <p><i class="fa fa-envelope"></i><span>Mail:</span>squarehospital@gmail.com</p>
                   </div>
                 </div>
                 <div class="single-hospital text-center">
                   <div class="h-hospital-content-top">
                     <img src="{{asset('visitor/img/hospital/hospital1.jpg')}}" alt="">
                   </div>
                   <div class="h-hospital-content-bottom">
                     <h2>Square Hospital</h2>
                     <p>West Panthopath, Dhaka</p>
                     <p><i class="fa fa-mobile"></i><span>Contact:</span>+8801939886956</p>
                     <p><i class="fa fa-envelope"></i><span>Mail:</span>squarehospital@gmail.com</p>
                   </div>
                 </div>
               </div>
              </div>
            </div>
            <!-- Hospital-list end-->
         </div>
       </div>
     </div>
   </div>

 </section>
<!-- hospitallist start -->
<div class="hospitallist-area">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="single-hospital-wrapper">
          <div class="hospital-img">
            <img src="{{asset('visitor/img/hospital/hospital1.jpg')}}" alt="">
          </div>
          <div class="hospital-details-mid">
            <h4>United Hospital</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente rerum, quis quod officia fugit adipisci!</p>
          </div>
          <div class="hospital-dis">
            <h4>Distance <br>10.5km</h4>
          </div>
          <div class="hos-details-right">
            <a href="singlehospital.html">View</a>
          </div>
        </div>
            <div class="single-hospital-wrapper">
          <div class="hospital-img">
            <img src="{{asset('visitor/img/hospital/hospital1.jpg')}}" alt="">
          </div>
          <div class="hospital-details-mid">
            <h4>United Hospital</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente rerum, quis quod officia fugit adipisci!</p>
          </div>
          <div class="hospital-dis">
            <h4>Distance <br>10.5km</h4>
          </div>
          <div class="hos-details-right">
            <a href="singlehospital.html">View</a>
          </div>
        </div>
            <div class="single-hospital-wrapper">
          <div class="hospital-img">
            <img src="{{asset('visitor/img/hospital/hospital1.jpg')}}" alt="">
          </div>
          <div class="hospital-details-mid">
            <h4>United Hospital</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente rerum, quis quod officia fugit adipisci!</p>
          </div>
          <div class="hospital-dis">
            <h4>Distance <br>10.5km</h4>
          </div>
          <div class="hos-details-right">
            <a href="singlehospital.html">View</a>
          </div>
        </div>
            <div class="single-hospital-wrapper">
          <div class="hospital-img">
            <img src="{{asset('visitor/img/hospital/hospital1.jpg')}}" alt="">
          </div>
          <div class="hospital-details-mid">
            <h4>United Hospital</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente rerum, quis quod officia fugit adipisci!</p>
          </div>
          <div class="hospital-dis">
            <h4>Distance <br>10.5km</h4>
          </div>
          <div class="hos-details-right">
            <a href="singlehospital.html">View</a>
          </div>
        </div>
            <div class="single-hospital-wrapper">
          <div class="hospital-img">
            <img src="{{asset('visitor/img/hospital/hospital1.jpg')}}" alt="">
          </div>
          <div class="hospital-details-mid">
            <h4>United Hospital</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente rerum, quis quod officia fugit adipisci!</p>
          </div>
          <div class="hospital-dis">
            <h4>Distance <br>10.5km</h4>
          </div>
          <div class="hos-details-right">
            <a href="singlehospital.html">View</a>
          </div>
        </div>
      </div>
    </div>
    <div class="np-area text-center">
   <a href="#"><<</a>
   <a href="#" id="active">1</a>
   <a href="#">2</a>
   <a href="#">3</a>
   <a href="#">4</a>
   <a href="#">5</a>
   <a href="#">>></a>
 </div>
  </div>

</div> 
 
<!-- footer-area -->
@stop