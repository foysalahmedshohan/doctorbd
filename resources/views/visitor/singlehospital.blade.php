@extends('visitor.layout.master')


@section('content')
<!-- single hospital start -->
 <div class="single-doctor">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="single-doc-top text-center">
          <img src=" {{ asset('visitor/img/1.jpg' )}} " alt="">
          <h2>United Hospital Bangladesh</h2>
          <h3>We are always ready to give you the best treatment.Come and get well soon.</h3>
          <h4><span>Dhaka,Bangladesh</span></h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
       <div class="single-doc-left text-center">
        <h3>Seat Availibility</h3>
        <li>ICU(<b>10</b>)</li>
        <li>CCU(<b>10</b>)</li>
        <li>HDU(<b>10</b>)</li>
        <li>NICU(<b>10</b>)</li>
        <li>PCU(<b>10</b>)</li>
       </div>
       <div class="single-doc-lb text-center" id="tcost">
         <h2>Test Cost</h2>
          <label for="">Test Name</label>
          <input type="text" name="t_name" placeholder="Enter test name">
          <h3>Some Test name and cost given below</h3>
          <table>
            <tr><th>Test Name</th><th>Cost</th></tr>
            <tr><td>Urine</td><td>400tk</td></tr>
            <tr><td>Ucg</td><td>300tk</td></tr>
            <tr><td>Chest X-ray</td><td>1000tk</td></tr>
            <tr><td>Mri</td><td>11000tk</td></tr>
            <tr><td>Blood</td><td>1000tk</td></tr>
          </table>
       </div>
      </div>
      <div class="col-md-6">
        <div class="chamber-lacation">
          <h2><i class="fa fa-location-arrow" aria-hidden="true"></i>Hospital Location</h2>
           <div class="map">
             <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.8445831279155!2d90.37948291428786!3d23.752920984588133!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8ae4e52eabd%3A0x113b1873c9a9c2c1!2sSquare+Hospital!5e0!3m2!1sen!2sbd!4v1548296476727" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
           </div>
        </div>

         <!-- book an appoinment -->
         <div class="appoinment-area">
            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Contact with Hospital Authority</button>
            <div id="demo" class="collapse">
            <h2>Contact with Authority</h2>
              <form action="">
                <table>
                <tr><td>Name</td><td><input type="text" placeholder="Enter Your Name"></td></tr>
                <tr><td>Email</td><td><input type="text" placeholder="Enter Your Email"></td></tr>
                <tr><td>Mobile No:</td><td><input type="text" placeholder="+8801xxxxxxxx"></td></tr>
                <tr><td></td><td><input type="submit" value="Send a Message"></td></tr>
              </table>
              </form>
            </div>
             </div>
            </div>
          </div>
        </div>
      </div>
<!-- footer-area -->
<div class="footer-area">
  <div class="container">
   <div class="row">
     <div class="col-md-4">
       <div class="footer-left">
         <h2>Let’s join & create something together</h2>
         <p>Must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and will give you completed.</p>
         <label for=""><input type="text" placeholder="Enter Your Email Address"></label>
         <a href="#">Subscribe us</a>
       </div>    
     </div>
          <div class="col-md-4">
       <div class="footer-midle d-flex">
         <h2>Usefull Links</h2>
        <div class="footer-midle-left">
           <li><a href="#">About company</a></li>
           <li><a href="#">About company</a></li>
           <li><a href="#">About company</a></li>
           <li><a href="#">About company</a></li>
           <li><a href="#">About company</a></li>
          
        </div>
        <div class="footer-midle-left">
              <li><a href="#">About company</a></li>
             <li><a href="#">About company</a></li>
             <li><a href="#">About company</a></li>
             <li><a href="#">About company</a></li>
             <li><a href="#">About company</a></li>
            
        </div>
         
       </div>    
     </div>
          <div class="col-md-4">
       <div class="footer-right">
         <h2>For business dealing</h2>
         <h2>888-000-1234</h2>
         <h3>Available 9:00 - 7:00 all days</h3>
         <h3>Big Smart Stareet, 2nd Cross 
            Newyork, USA 1002.</h3>
          <hr>
           <h2>For business dealing</h2>
         <h2>888-000-1234</h2>
        
       </div>    
     </div>
   </div>
    
  </div>
</div>
<!--==================================================================-->
<script type="text/javascript" src="assets/js/jquery-3.2.0.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
<!--=== All Plugin ===-->
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/plugin/wow.min.js"></script>
<script type="text/javascript" src="assets/js/plugin/jquery.sticky.js"></script>
<script type="text/javascript" src="assets/js/plugin/jquery.scrolly.js"></script>
<script type="text/javascript" src="assets/js/plugin/nivo-slider.js"></script>
<script type="text/javascript" src="assets/js/plugin/jquery.meanmenu.min.js"></script>
<!-- counter-area  start-->
<script type="text/javascript" src="assets/js/plugin/jquery.counterup.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<!-- counter-area end -->
<!-- nivo-slider -->
<script type="text/javascript" src="assets/js/plugin/home.js"></script>
<!-- <script type="text/javascript" src="assets/js/carousel/owl.carousel.min.js"></script> -->
<script type="text/javascript" src="assets/js/plugin/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/plugin/jquery.magnific-popup.min.js"></script>

<!--=== All Active ===-->
<script type="text/javascript" src="assets/js/main.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  e.src='https://www.google-analytics.com/analytics.js';
  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
<script>
  $(document).ready(function(){
   $('#menu').slicknav();
   $(".header_area").sticky({topSpacing:0});

 });

</script>
</body>
</html>


@stop