@extends('visitor.layout.master')


@section('content')
 <!-- end of header area -->
 <!-- search blood area start-->
 <div class="b-search-area">
  <h1>I'm looking for a educational video</h1>
   <div class="container">
     <div class="row">
       <div class="col-md-2">   
       </div>
       <div class="col-md-8">
         <div class="doctor-srchbar">
           <form class="form-inline">
            <label class="sr-only" for="inlineFormInputName2">City</label>
            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Enter City">

            <label class="sr-only" for="inlineFormInputGroupUsername2">Area</label>
            <div class="input-group mb-2 mr-sm-2">
              <div class="input-group-prepend">
              </div>
              <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Area">
            </div>
              <label class="sr-only" for="inlineFormInputGroupUsername2">Blood Group</label>
            <div class="input-group mb-2 mr-sm-2">
              <div class="input-group-prepend">
              </div>
              <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Blood Group">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Search</button>
          </form>
         </div>
       </div>
       <div class="col-md-2"></div>
     </div>
   </div>
  </div>
 <!-- search blood area end-->
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="doctor-slide-area">
           <h3>You also can choose donor form below</h3>
           <!-- Hospital-list start-->
            <div class="hospital-area-list pt-40 pb-80">
             <div class="container">
               <div class="row">
                <div class="hospital-slider">
                 <div class="single-hospital text-center">
                   <div class="h-hospital-content-top">
                    <img src=" {{ asset('visitor/img/1.jpg' )}} " >
                   </div>
                   <div class="h-hospital-content-bottom">
                    
                    <h2>Taijul islam</h2>
                     <span>Blood Group: O+(ev)</span>
                    <h3>I am always ready to donate my blood without any condition and payment</h3>
                    <h4><span>01772488816</span></h4>
                    <a href="signledonor.html">View Details</a>
                   </div>
                 </div>
                   <div class="single-hospital text-center">
                   <div class="h-hospital-content-top">
                    <img src=" {{ asset('visitor/img/1.jpg' )}} " alt="">
                   </div>
                   <div class="h-hospital-content-bottom">
                    
                    <h2>Taijul islam</h2>
                     <span>Blood Group: O+(ev)</span>
                    <h3>I am always ready to donate my blood without any condition and payment</h3>
                    <h4><span>01772488816</span></h4>
                    <a href="signledonor.html">View Details</a>
                   </div>
                 </div>
                   <div class="single-hospital text-center">
                   <div class="h-hospital-content-top">
                    <img src=" {{ asset('visitor/img/1.jpg' )}} " alt="">
                   </div>
                   <div class="h-hospital-content-bottom">
                    
                    <h2>Taijul islam</h2>
                     <span>Blood Group: O+(ev)</span>
                    <h3>I am always ready to donate my blood without any condition and payment</h3>
                    <h4><span>01772488816</span></h4>
                    <a href="signledonor.html">View Details</a>
                   </div>
                 </div>
                  <div class="single-hospital text-center">
                   <div class="h-hospital-content-top">
                    <img src=" {{ asset('visitor/img/1.jpg' )}} " alt="">
                   </div>
                   <div class="h-hospital-content-bottom">
                    
                    <h2>Taijul islam</h2>
                     <span>Blood Group: O+(ev)</span>
                    <h3>I am always ready to donate my blood without any condition and payment</h3>
                    <h4><span>01772488816</span></h4>
                    <a href="signledonor.html">View Details</a>
                   </div>
                 </div>
                 <div class="single-hospital text-center">
                   <div class="h-hospital-content-top">
                    <img src=" {{ asset('visitor/img/1.jpg' )}} " alt="">
                   </div>
                   <div class="h-hospital-content-bottom">
                    
                    <h2>Taijul islam</h2>
                     <span>Blood Group: O+(ev)</span>
                    <h3>I am always ready to donate my blood without any condition and payment</h3>
                    <h4><span>01772488816</span></h4>
                    <a href="signledonor.html">View Details</a>
                   </div>
                 </div>
               </div>
              </div>
            </div>
            </div>
            <!-- Hospital-list end-->
         </div>
       </div>
     </div>
   </div>

 <div class="container">
  <div class="row b_search text-center">
    <h2>Your Video Is Here</h2>

  <div class="col-md-12">
    <div class="col-md-6">
      <iframe width="100%" height="345" src="https://www.youtube.com/embed/tgbNymZ7vqY">
        </iframe>
    </div>

    <div class="col-md-6 ">
      <iframe width="100%" height="345" src="https://www.youtube.com/embed/tgbNymZ7vqY">
      </iframe>
    </div>

  </div>
  </div>
 <div class="np-area text-center">
   <a href="#"><<</a>
   <a href="#" id="active">1</a>
   <a href="#">2</a>
   <a href="#">3</a>
   <a href="#">4</a>
   <a href="#">5</a>
   <a href="#">>></a>
 </div>
 </div>
 <!-- footer-area -->

@stop