<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hospital extends Model
{
    protected $fillable=['hname','phone','aphone','image','email','address','status','longitude','latitude','password'];
}
