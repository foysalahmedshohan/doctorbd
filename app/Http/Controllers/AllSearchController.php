<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Ambulance;
use App\Hospital;
use App\Doctor;
use App\Donor;
use App\User;
use Toastr;

class AllSearchController extends Controller
{
    //

	public function doctorSearch(Request $request)
	{
        $term = $request->term;
        $doctors = User::join('doctors', 'users.id', '=', 'doctors.user_id')->where('name', 'LIKE', '%'.$term.'%')->get();
        $specialties = \DB::table('doctor_specialty')->where('specialty_name', 'LIKE', '%'.$term.'%')->get();

        if (count($doctors) == 0 and count($specialties) == 0) {
            $search_result[] = "No Result Found";
        }
        else{
            foreach ($specialties as $specialty) {
                $search_result[] = $specialty->specialty_name;
            }

            foreach ($doctors as $doctor) {
                $search_result[] = $doctor->name;
            }
        }
        return $search_result;
		
    }

    public function ambulanceSearch(Request $request)
    {
    	$ckResult = 0;
    	
    	if (isset($request->district) or isset($request->type)) {
		   
		    $ckResult = 1;
		    $latest_amb = Ambulance::latest()->take(5)->get();
    		// dd($request->district);
    		if (isset($request->district) && isset($request->type)) {
    			
    			$total_ambulance = Ambulance::where('district', $request->district)
							->where('type', $request->type)
							->count();

    			$ambulances = Ambulance::where('district', $request->district)
				 		 ->where('type', $request->type)
			    		 ->paginate(4);
    		}
    		else{
    			$total_ambulance = Ambulance::where('district', $request->district)
							->orWhere('type', $request->type)
							->count();

    			$ambulances = Ambulance::where('district', $request->district)
				 		 ->orWhere('type', $request->type)
			    		 ->paginate(4);
    		}
    		

    		$ambulances->appends(['district' => $request->district, 'type'=> $request->type ]);

    		return view('visitor.ambsearch', compact('ambulances','latest_amb','total_ambulance','ckResult'));
    	}
    	else{
    		$ckResult = 0;
    		$latest_amb = Ambulance::latest()->take(5)->get();
    		
    		return view('visitor.ambsearch', compact('latest_amb','ckResult'));
    	}
    	
    }

    //Blood donor search
    public function donorSearch(Request $request)
    {
    	$ckResult = 0;
    	
    	if (isset($request->district) or isset($request->bg)) {
		   
		    $ckResult = 1;
		    $latest_donor = Donor::latest()->take(5)->get();
    		// dd($request->district);
    		if (isset($request->district) && isset($request->bg)) {
    			
    			$total_donor = Donor::where('district', $request->district)
							->where('bg', $request->bg)
							->count();

    			$donors = Donor::where('district', $request->district)
				 		 ->where('bg', $request->bg)
			    		 ->paginate(3);
    		}
    		else{
    			$total_donor = Donor::where('district', $request->district)
							->orWhere('bg', $request->bg)
							->count();

    			$donors = Donor::where('district', $request->district)
				 		 ->orWhere('bg', $request->bg)
			    		 ->paginate(3);
    		}
    		

    		$donors->appends(['district' => $request->district, 'bg'=> $request->bg ]);
    		return view('visitor.donor_search', compact('donors','latest_donor','total_donor','ckResult'));
    	}
    	else{
    		$ckResult = 0;
    		$latest_donor = Donor::latest()->take(5)->get();
    		
    		return view('visitor.donor_search', compact('latest_donor','ckResult'));
    	}
    }




// end of controller
}
